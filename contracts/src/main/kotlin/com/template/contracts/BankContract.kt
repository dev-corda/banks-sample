package com.template.contracts

import com.template.states.BankState
import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.Requirements.using
import net.corda.core.transactions.LedgerTransaction

// ************
// * Contract *
// ************
class BankContract : Contract {
    companion object {
        // Used to identify our contract when building a transaction.
        const val ID = "com.template.contracts.BankContract"
    }
    class Create : CommandData
    // A transaction is valid if the verify() function of the contract of all the transaction's input
    // and output states does not throw an exception.
    override fun verify(tx: LedgerTransaction) {
        // Verification logic goes here.
        val command = tx.commands.first()

        if (command.value is Commands.Request) {
            "There should be only one output state." using (tx.outputs.size == 1)
            val output = tx.outputs.single().data
            "This must be a financial transaction." using (output is BankState)
            val loanRequest = output as BankState
//            "The transfer value should be grater than zero." using (loanRequest.requestValue > 0)
//            "The transfer value should be less than 9 Crore." using (loanRequest.requestValue < 900000000)
        }
    }

    // Used to indicate the transaction's intent.
    interface Commands : CommandData {
        class Request : Commands
        class ApproveLoan : Commands
    }
}
