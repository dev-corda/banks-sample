package com.template.states

import com.template.contracts.BankContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party

// *********
// * State *
// *********
@BelongsToContract(BankContract::class)
class BankState(val TransferValue: Int,
                val Sender: Party,
                val Reciever: Party,
                override val linearId: UniqueIdentifier = UniqueIdentifier())
 	: LinearState {
        override val participants get() = listOf(Sender, Reciever)
}



